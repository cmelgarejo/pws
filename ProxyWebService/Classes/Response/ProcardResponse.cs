﻿using ProxyWebService.Classes.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

namespace ProxyWebService.Classes
{
    public class ProcardResponse
    {
        public bool success { get; set; }
        public string response_code { get; set; }
        public string response_message { get; set; }
        public string original_procard_request { get; set; }
        public string original_procard_response { get; set; }
        public Dictionary<string, string> result { get; set; }
        IBaseProcardRequestClass _request;


        public ProcardResponse(IBaseProcardRequestClass request)
        {
            _request = request;
            original_procard_request = _request.ProcardMessage();
            success = false;
            response_code = ConfigurationManager.AppSettings["Procard_Response_Default"];
        }

        public ProcardResponse(IBaseProcardRequestClass request, string message)
        {
            _request = request;
            original_procard_request = _request.ProcardMessage();
            success = false;
            response_code = ConfigurationManager.AppSettings["Procard_Response_Default"];
            Parse(message);
        }

        public void Parse(string strMsg)
        {
            try
            {
                bool consultaRequest = false;
                original_procard_response = strMsg;
                string[] split_result = strMsg.Split(ConfigurationManager.AppSettings["ProcardMessageSeparator"].ToCharArray());
                response_code = split_result.First().PadLeft(2, '0');
                if (split_result.Length > 1)
                {
                    if (_request.GetType() == typeof(ConsultaRequest) && response_code != ConfigurationManager.AppSettings["Procard_Response_Default"])
                    {
                        consultaRequest = true;
                        response_code = ConfigurationManager.AppSettings["Procard_Response_Accepted"];
                    }                        
                    if (response_code == ConfigurationManager.AppSettings["Procard_Response_Accepted"])
                        success = true;
                    response_message = ConfigurationManager.AppSettings[string.Format("Procard_Response_{0}", response_code)];
                }
                if (string.IsNullOrEmpty(response_message))
                    response_message = original_procard_response;
                if (consultaRequest)
                {
                    var consultaReq = (ConsultaRequest)_request;
                    //Best course of action: Refactor to store the Regex pattern in a setting in web.config.
                    string pattern = ".{8}#.{4}#.{30}#.{2}#.{41}#.{17}#.{17}";
                    Match match = Regex.Match(original_procard_response, pattern, RegexOptions.Singleline);
                    while (match.Success)
                    {
                        split_result = match.Value.Split(ConfigurationManager.AppSettings["ProcardMessageSeparator"].ToCharArray());
                        result = _request.ResponseParams().Zip(split_result, (key, value) => new { key, value })
                                 .ToDictionary(kv => kv.key, kv => kv.value.Replace("\r\n", string.Empty).Trim());
                        if (result[ConfigurationManager.AppSettings["ProcardOperation_Consulta_OutParam_02"]] == consultaReq.card_ending &&
                            result[ConfigurationManager.AppSettings["ProcardOperation_Consulta_OutParam_04"]] == consultaReq.card_class)
                            return;                            
                        match = match.NextMatch();
                    }
                    success = false;
                    response_message = "CARD_NOT_FOUND";
                    response_code = ConfigurationManager.AppSettings["Procard_Response_Default"];
                    result = new Dictionary<string, string>();
                }
                else
                {
                    result = _request.ResponseParams().Zip(split_result.Skip(1), (key, value) => new { key, value })
                                                     .ToDictionary(kv => kv.key, kv => kv.value.Replace("\r\n", string.Empty).Trim());
                }
            }
            catch (Exception ex)
            {
                response_code = ConfigurationManager.AppSettings["Procard_Response_Default"];
                success = false;
                response_message = ex.Message;
            }
        }
    }
}